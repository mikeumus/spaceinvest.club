
function addDealLink(titleElem, addedHref) {
    var targetElemt = $(titleElem);
    var tartar = $(targetElemt.siblings()[2]).find('.live-deal-btn');

    tartar.attr('href', addedHref);
    tartar.attr('target','_blank');
}

function addToShopify() {
var shopFrame = $('[name="frame-productSet-264533573793"]');

shopFrame.contents().find(".shopify-buy__collection").append('<style>a { text-decoration: none} .added-shop-btn {font-size: 15px;padding: 12px 40px;letter-spacing: .3px;border-radius: 3px;cursor: pointer;transition: background 200ms ease;max-width: 100%;text-overflow: ellipsis;overflow: hidden;line-height: 1.2;text-align: center;-webkit-appearance: none;} .live-deal-btn {border: 2px solid #2a2f4a;width: 135px;height: 42px;flex-wrap: wrap;display: flex;margin-top: 7px !important;background: #fff;color: #2a2f4a;padding: 10px 23px; margin: auto} .live-deal-btn:hover {background: #fff}</style>');

shopFrame.contents().find('.shopify-buy__btn-wrapper').append('<a class="added-shop-btn live-deal-btn">Live Deal ↗ </a>');

var reportTitles = shopFrame.contents().find('.shopify-buy__product__title');

for (var i = 0; i < reportTitles.length; i++) {
    if (reportTitles[i].innerText.includes("Unistellar")) {
        addDealLink(reportTitles[i], 'https://www.fundable.com/unistellar');
    }
    if (reportTitles[i].innerText.includes("Rogue Space")) {
        addDealLink(reportTitles[i], 'https://us.trucrowd.com/equity/offer-summary/Rogue');
    }
    if (reportTitles[i].innerText.includes("Hudson Space")) {
        addDealLink(reportTitles[i], 'https://invest.microventures.com/offerings/hudson-space-systems');
    }
    if (reportTitles[i].innerText.includes("bluShift")) {
        addDealLink(reportTitles[i], 'https://wefunder.com/blushift.aerospace');
    }
}

}

/// Reference: https://stackoverflow.com/a/3403063/1762493
function timerAddDeals () {
    setTimeout (function check_trigger () {
      if ( $('[name="frame-productSet-264533573793"]').length == 0 ) {
        // $('#mc_signup_form .loading').remove();
        setTimeout(addToShopify, 1333);
      } else {
        addToShopify();
        return;
      }   
    }, 1333); 
}

timerAddDeals();
